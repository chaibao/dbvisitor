<?xml version="1.0" encoding="UTF-8" ?>
<!--
       Copyright 2009-2018 the original author or authors.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
-->
<!ELEMENT mapper ( resultMap* | sql* | insert* | update* | delete* | select* )+>
<!ATTLIST mapper
        namespace CDATA #IMPLIED
        caseInsensitive (true|false) #IMPLIED
        mapUnderscoreToCamelCase (true|false) #IMPLIED
        autoMapping (true|false) #IMPLIED
        >

<!ELEMENT resultMap (id*, result*)>
<!ATTLIST resultMap
        type CDATA #REQUIRED
        id CDATA #IMPLIED
        schema CDATA #IMPLIED
        table CDATA #IMPLIED
        caseInsensitive (true|false) #IMPLIED
        mapUnderscoreToCamelCase (true|false) #IMPLIED
        autoMapping (true|false) #IMPLIED
        >

<!ELEMENT id EMPTY>
<!ATTLIST id
        property CDATA #REQUIRED
        javaType CDATA #IMPLIED
        column CDATA #REQUIRED
        jdbcType CDATA #IMPLIED
        typeHandler CDATA #IMPLIED
        >

<!ELEMENT result EMPTY>
<!ATTLIST result
        property CDATA #REQUIRED
        javaType CDATA #IMPLIED
        column CDATA #REQUIRED
        jdbcType CDATA #IMPLIED
        typeHandler CDATA #IMPLIED
        >

<!ELEMENT mapping EMPTY>
<!ATTLIST mapping
        property CDATA #REQUIRED
        javaType CDATA #IMPLIED
        column CDATA #REQUIRED
        jdbcType CDATA #IMPLIED
        typeHandler CDATA #IMPLIED
        >

<!ELEMENT insert (#PCDATA | selectKey | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST insert
        id CDATA #REQUIRED
        statementType (STATEMENT|PREPARED|CALLABLE) #IMPLIED
        timeout CDATA #IMPLIED
        >

<!ELEMENT update (#PCDATA | selectKey | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST update
        id CDATA #REQUIRED
        statementType (STATEMENT|PREPARED|CALLABLE) #IMPLIED
        timeout CDATA #IMPLIED
        >

<!ELEMENT selectKey (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST selectKey
        statementType (STATEMENT|PREPARED|CALLABLE) #IMPLIED
        timeout CDATA #IMPLIED
        resultType CDATA #IMPLIED
        fetchSize CDATA #IMPLIED
        resultSetType CDATA #IMPLIED
        keyProperty CDATA #REQUIRED
        keyColumn CDATA #IMPLIED
        order (BEFORE|AFTER) #IMPLIED
        handler CDATA #IMPLIED
        >

<!ELEMENT delete (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST delete
        id CDATA #REQUIRED
        statementType (STATEMENT|PREPARED|CALLABLE) #IMPLIED
        timeout CDATA #IMPLIED
        >

<!ELEMENT select (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST select
        id CDATA #REQUIRED
        statementType (STATEMENT|PREPARED|CALLABLE) #IMPLIED
        timeout CDATA #IMPLIED
        resultMap CDATA #IMPLIED
        resultType CDATA #IMPLIED
        fetchSize CDATA #IMPLIED
        resultSetType (FORWARD_ONLY | SCROLL_INSENSITIVE | SCROLL_SENSITIVE | DEFAULT) #IMPLIED
        multipleResult (FIRST|LAST|ALL) #IMPLIED
        >

<!-- Dynamic -->
<!ELEMENT sql (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST sql
        id CDATA #REQUIRED
        >

<!ELEMENT include EMPTY>
<!ATTLIST include
        refid CDATA #REQUIRED
        >

<!ELEMENT trim (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST trim
        prefix CDATA #IMPLIED
        prefixOverrides CDATA #IMPLIED
        suffix CDATA #IMPLIED
        suffixOverrides CDATA #IMPLIED
        >

<!ELEMENT where (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>

<!ELEMENT set (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>

<!ELEMENT foreach (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST foreach
        collection CDATA #REQUIRED
        item CDATA #REQUIRED
        index CDATA #IMPLIED
        open CDATA #IMPLIED
        close CDATA #IMPLIED
        separator CDATA #IMPLIED
        >

<!ELEMENT choose (when* , otherwise?)>

<!ELEMENT when (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST when
        test CDATA #REQUIRED
        >

<!ELEMENT otherwise (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>

<!ELEMENT if (#PCDATA | include | trim | where | set | foreach | choose | if | bind)*>
<!ATTLIST if
        test CDATA #REQUIRED
        >

<!ELEMENT bind EMPTY>
<!ATTLIST bind
        name CDATA #REQUIRED
        value CDATA #REQUIRED
        >